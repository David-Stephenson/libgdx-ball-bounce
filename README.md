# Ball Bounce

This is a simple project created with Java and [LibGDX](https://libgdx.com/).

The ball will bounce off all corners of the screen.

The goal of this project was to give me a basic understanding of LibGDX.

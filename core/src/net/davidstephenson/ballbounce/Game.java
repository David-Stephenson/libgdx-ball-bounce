package net.davidstephenson.ballbounce;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.utils.ScreenUtils;

public class Game extends ApplicationAdapter {
	Circle ball;
	ShapeRenderer shape;
	int ballXSpeed = 5;
	int ballYSpeed = 5;

	@Override
	public void create() {
		ball = new Circle(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 25);
		shape = new ShapeRenderer();
	}

	public void render() {
		ScreenUtils.clear(0, 0, 0, 1);

		// * Draw ball
		shape.begin(ShapeType.Filled);
		shape.setColor(Color.RED);
		shape.circle(ball.x, ball.y, ball.radius);
		shape.end();

		// * Ball movement

		ball.x += ballXSpeed;
		ball.y += ballYSpeed;

		// * Y Axis bounce
		if (ball.y + 25 >= Gdx.graphics.getHeight()) {
			ballYSpeed = -ballYSpeed;
		}

		if (ball.y - 25 <= 0) {
			ballYSpeed = -ballYSpeed;
		}

		// * X Axis bounce
		if (ball.x + 25 >= Gdx.graphics.getWidth()) {
			ballXSpeed = -ballXSpeed;
		}

		if (ball.x - 25 <= 0) {
			ballXSpeed = -ballXSpeed;
		}
	}
}